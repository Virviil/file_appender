-module(file_appender_SUITE).

-include_lib("common_test/include/ct.hrl").

-export([all/0]).

-export([end_per_testcase/2, init_per_testcase/2]).

-export([creates_appender_worker_process/1,
	 reopens_file_on_worker_death/1]).

all() ->
    [creates_appender_worker_process,
     reopens_file_on_worker_death].

init_per_testcase(CaseName, Config) ->
    FilePath = "/tmp/" ++ atom_to_list(CaseName),
    file:delete(FilePath),
    application:ensure_all_started(file_appender),
    [{file, FilePath} |Config].

end_per_testcase(CaseName, Config) ->
    file:delete("/tmp/" ++ atom_to_list(CaseName)), Config.

creates_appender_worker_process(Config) ->
    ok = file_appender:append_safe(?config(file, Config), "data"),
    Pid = gproc:lookup_local_name({file_appender_server,
				   ?config(file, Config)}),
    true = is_process_alive(Pid),
    {ok, <<"data\n">>} = file:read_file(?config(file, Config)).

reopens_file_on_worker_death(Config) ->
    ok = file_appender:append_safe(?config(file, Config), "data"),
    Pid = gproc:lookup_local_name({file_appender_server,
				   ?config(file, Config)}),
    timer:sleep(10000),
    false = is_process_alive(Pid),
    ok = file_appender:append_safe(?config(file, Config), "data2"),
    NewPid = gproc:lookup_local_name({file_appender_server,
				      ?config(file, Config)}),
    false = Pid == NewPid,
    {ok, <<"data\ndata2\n">>} = file:read_file(?config(file, Config)).
