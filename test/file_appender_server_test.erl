-module(file_appender_server_test).

-include_lib("eunit/include/eunit.hrl").

-define(TMP_FILE, "/tmp/file_appender_server_test").

basic_attitude_test_() ->
    {foreach, fun start/0, fun stop/1,
     [fun is_registered/1, fun is_dieing_after_inactivity/1,
      fun is_creating_new_file/1,
      fun is_writing_to_file_sync/1,
      fun is_putting_data_to_new_line_sync/1,
      fun is_not_accepting_bad_data_sync/1,
      fun is_writing_to_file_async/1,
      fun is_putting_data_to_new_line_async/1,
      fun is_not_accepting_bad_data_async/1]}.

start() ->
    application:ensure_all_started(gproc),
    {ok, Pid} = file_appender_server:start_link({?TMP_FILE,
						 100}),
    Pid.

stop(Pid) ->
    file:delete(?TMP_FILE),
    case erlang:is_process_alive(Pid) of
      true -> file_appender_server:stop(Pid);
      _ -> skip
    end.

is_registered(Pid) ->
    [?_assert((erlang:is_process_alive(Pid))),
     ?_assertEqual(Pid,
		   (gproc:lookup_local_name({file_appender_server,
					     ?TMP_FILE})))].

is_dieing_after_inactivity(Pid) ->
    timer:sleep(150),
    ?_assert((not erlang:is_process_alive(Pid))).

is_creating_new_file(_Pid) ->
    ?_assert((filelib:is_regular(?TMP_FILE))).

is_writing_to_file_sync(Pid) ->
    [?_assertMatch(ok,
		   (file_appender_server:append_safe(Pid, "Test data"))),
     ?_assertMatch({ok, <<"Test data\n">>},
		   (file:read_file(?TMP_FILE)))].

is_putting_data_to_new_line_sync(Pid) ->
    file_appender_server:append_safe(Pid, "Test data"),
    file_appender_server:append_safe(Pid, "Test data 2"),
    ?_assertMatch({ok, <<"Test data\nTest data 2\n">>},
		  (file:read_file(?TMP_FILE))).

is_not_accepting_bad_data_sync(Pid) ->
    [?_assertMatch({error, badarg},
		   (file_appender_server:append_safe(Pid, 12345))),
     ?_assertMatch({ok, <<>>}, (file:read_file(?TMP_FILE)))].

is_writing_to_file_async(Pid) ->
    [?_assertMatch(ok,
		   (file_appender_server:append_fast(Pid, "Test data"))),
     ?_assertMatch({ok, <<"Test data\n">>},
		   (file:read_file(?TMP_FILE)))].

is_putting_data_to_new_line_async(Pid) ->
    file_appender_server:append_fast(Pid, "Test data"),
    file_appender_server:append_fast(Pid, "Test data 2"),
    timer:sleep(100),
    ?_assertMatch({ok, <<"Test data\nTest data 2\n">>},
		  (file:read_file(?TMP_FILE))).

is_not_accepting_bad_data_async(Pid) ->
    [?_assertMatch({error, badarg},
		   (file_appender_server:append_fast(Pid, 12345))),
     ?_assertMatch({ok, <<>>}, (file:read_file(?TMP_FILE)))].


perform_appends_not_rewrite_file_test() ->
    Pid1 = start(),
    file_appender_server:append_safe(Pid1, "First"),
    file_appender_server:stop(Pid1),
    Pid2 = start(),
    file_appender_server:append_safe(Pid2, "Second"),
    FData = file:read_file(?TMP_FILE),
        file:delete(?TMP_FILE),
    ?assertMatch({ok, <<"First\nSecond\n">>}, FData).
