# file_appender

File appender OTP application

## Basic description

File appender appends passed data to a file:

* Creates a file if it not exists;
* Opens file in append mode, if it exists.

Appending is performed in two modes:

* **safe** - sync. This mode ensures that the data will be written in the file upon returning true.
* **fast** - async. This mode ensures, that the execution flow will not be blocked by appending.

Upon small load it's better to use **safe** version of the API.

After 10 seconds, file descriptior will be freed, and file will be closed.
It can be reopened without any additional steps - with the same API.

## Usage

```erlang
application:ensure_all_started(file_appender).

ok = file_appender:append_safe("/path/to/my/file", "data").
ok = file_appender:append_fast("path/to/my/file", "new data").
```


## Dependencies

`gproc` is used as a local process registry

## Build

    $ rebar3 compile

## Test

Unit tests upon `file_appender` worker gen_server:

    $ rebar3 eunit

Integration tests for `file_appender` application:

    $ rebar3 ct