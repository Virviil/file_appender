%%%-------------------------------------------------------------------
%% @doc file_appender top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(file_appender_sup).

-behaviour(supervisor).

-export([start_link/0, start_child/1]).

-export([init/1]).

-define(SERVER, ?MODULE).

% Ensuring, that if all the processes will close at the same time, supervisor will survive.
-define(MAX_FILE_DESCRIPTORS, 1024).
% In seconds - timeout to close the file.
-define(TIMEOUT, 10).

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

start_child(FilePath) ->
    supervisor:start_child(?SERVER, [{FilePath, ?TIMEOUT*1000}]).

%% Callbacks

init([]) ->
    % Simple one_for_one supervision mode is used to start and stop workers dynamicly.
    SupFlags = #{strategy => simple_one_for_one,
                 intensity => ?MAX_FILE_DESCRIPTORS,
                 period => ?TIMEOUT},
    ChildSpecs = [#{
        id => file_appender_server,
        start => {file_appender_server, start_link, []},
        shutdown => brutal_kill,
        restart => transient
    }],
    {ok, {SupFlags, ChildSpecs}}.

%% internal functions
