-module(file_appender).

-export([append_fast/2, append_safe/2]).

append_fast(FilePath, Data) ->
    do_append(FilePath, Data,
	      fun file_appender_server:append_fast/2).

append_safe(FilePath, Data) ->
    do_append(FilePath, Data,
	      fun file_appender_server:append_safe/2).

do_append(FilePath, Data, Appender) ->
    Appender(case file_appender_sup:start_child(FilePath) of
	       {ok, Pid} -> Pid;
	       {error, {already_started, Pid}} -> Pid;
           Err -> io:format("Error: ~p~n", [Err])
	     end,
	     Data).
