-module(file_appender_server).
-behaviour(gen_server).

%% API
-export([stop/1, start_link/1, append_fast/2, append_safe/2]).
%% Callbacks
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).


% Gproc is used as process register, in order to ensure worker uniqness by the file, that it holds.
via_tuple(FilePath) -> {via, gproc, {n, l, {file_appender_server, FilePath}}}.

%%% API
start_link({FilePath, Timeout}) ->
   gen_server:start_link(via_tuple(FilePath), ?MODULE, {FilePath, Timeout}, []).

stop(Name) ->
   gen_server:call(Name, stop).

% Function is nonblocking.
% It has garantees upon fail, but no garantees upon success.
% Can be used to write non-sensetive data.
append_fast(Name, Data) when is_list(Data) ->
    case is_process_alive(Name) of 
        true -> gen_server:cast(Name, {append, Data});
        _ -> {error, file_closed}
    end;
append_fast(_Name, _Data) -> {error, badarg}.

% Function is blocking.
% It has garantees upon success and fail.
% Can probably block an execution thread if write to file temporarly overloaded.
append_safe(Name, Data) when is_list(Data) ->
    gen_server:call(Name, {append, Data});
append_safe(_, _) -> {error, badarg}.

%%% Callbacks
init({FilePath, Timeout}) ->
    case file:open(FilePath, [append]) of
        {ok, Descriptor} -> {ok, {Descriptor, Timeout}, Timeout};
        {error, Reason} -> {stop, Reason}
    end.

handle_call({append, Data}, _From, State = {Descriptor, Timeout}) ->
    io:format(Descriptor, "~s~n", [Data]),
    {reply, ok, State, Timeout};
handle_call(stop, _From, State) ->
   {stop, normal, stopped, State};
handle_call(_Request, _From, State = {_Descriptor, Timeout}) ->
    {reply, ok, State, Timeout}.

handle_cast({append, Data}, State = {Descriptor, Timeout}) ->
    io:format(Descriptor, "~s~n", [Data]),
    {noreply, State, Timeout}.

handle_info(timeout, State) ->
    {stop, normal, State};
handle_info(_Info, State = {_Descriptor, Timeout}) ->
    {noreply, State, Timeout}.

terminate(_Reason, _State) ->
    % While the file descriptor process is linked to
    % current process, no need to close it manualy.
    ok.

%% Code change is not used here
code_change(_OldVsn, State, _Extra) ->
   {ok, State}.
